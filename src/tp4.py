import os
import numpy as np
import time
import matplotlib.pyplot as plt
from tp1 import load_adjacency_array
import random
import networkx
from sklearn.metrics import accuracy_score
from sklearn.metrics.cluster import normalized_mutual_info_score, adjusted_rand_score

seed = 18
np.random.seed(seed)

name = "generate" # "generate" or "amazon" / "lj" / "orkut" / "friendster"
plot = True

def generate_random_graph(p, q, n_nodes=400, n_cluster=4):
    assert q <= p, "q must be inferior to p"

    nodes = list(range(n_nodes))
    adjacency_array = dict()
    for x in nodes:
        x_cluster = x % n_cluster
        for y in range(x):
            connected = False
            if x_cluster == y % n_cluster:
                if random.random() <= p:
                    connected = True
            else:
                if random.random() <= q:
                    connected = True

            if connected:
                adjacency_array.setdefault(x, [])
                adjacency_array[x].append(y)
                adjacency_array.setdefault(y, [])
                adjacency_array[y].append(x)

    return adjacency_array


def label_propagation(graph):
    labels = {node: label for label, node in enumerate(graph)}
    order = list(labels.keys())
    np.random.shuffle(order)
    change = True
    while change:
        change = False
        for node in order:
            label = labels[node]
            neighbor_labels = {}
            for neighbor in graph[node]:
                if labels[neighbor] not in neighbor_labels:
                    neighbor_labels[labels[neighbor]] = 1
                else:
                    neighbor_labels[labels[neighbor]] += 1
            top_count = np.max(list(neighbor_labels.values()))
            top_labels = [label for label, count in neighbor_labels.items() if count == top_count]

            if label in top_labels:
                continue
            else:
                change = True
                labels[node] = top_labels[np.random.randint(0, len(top_labels))]
    return labels

def export_adjacency_array(adjacency_array, filename = "output.txt"):
    f = open(filename,"w")
    for k in adjacency_array.keys():
        for node in adjacency_array[k]:
            if k<node:
                f.write(f"{k} {node}\n")
    f.close()
                
def plot_louvain(graph_file,louvain_file):
    A = load_adjacency_array(graph_file)
    f = open(louvain_file,"r")
    clusters = []
    for line in f:
        clusters.append(int(line.split(" ")[-1]))
    f.close()
    G = networkx.Graph(A)
    
    clusters_sorted = []
    for k in A.keys():
        clusters_sorted.append(clusters[k])
    
    networkx.draw(G, node_size=50, arrowsize=5, node_color=clusters_sorted)
    plt.show()
    
def compute_louvain_scores(k):
    f = open(f"louvain_res/graph_{k}_out.txt","r")
    clusters = []
    for line in f:
        clusters.append(int(line.split(" ")[-1]))
    f.close()
    
    # we need to match the labels of the algorithm to the original one
    match = {}
    for i,c in enumerate(clusters):
        true_c = i%(4*2**k)
        match.setdefault(c,[])
        match[c].append(true_c)
    
    #for each of the new labels, we take the orig label with the more matches
    cluster_to_label = {}
    for c in match.keys():
        labels, count = np.unique(match[c], return_counts=True)
        label = labels[np.argmax(count)]
        cluster_to_label[c] = label
        
    label_true = []
    label_louvain = []
    for i,c in enumerate(clusters):
        label_true.append(i%(4*2**k))
        label_louvain.append(cluster_to_label[c])

    print(f"Louvain accuracy : {round(100*accuracy_score(label_true, label_louvain),2)}%")
    print(f"Louvain normalized mutual info score : {round(100*normalized_mutual_info_score(label_true, label_louvain),2)}%")
    print(f"Louvain rand score : {round(100*adjusted_rand_score(label_true, label_louvain),2)}%")
    
def compute_label_prop_scores(k):
    A = load_adjacency_array(f"generated_graph/graph_{k}.txt")
    labels = label_propagation(A)
    # we need to match the label of the algorithm to the original one
    match = {}
    for i in labels.keys():
        true_c = i%(4*2**k)
        c = labels[i]
        match.setdefault(c,[])
        match[c].append(true_c)
    
    #for each of the new labels, we take the orig label with the more matches
    cluster_to_label = {}
    for c in match.keys():
        labels2, count = np.unique(match[c], return_counts=True)
        label = labels2[np.argmax(count)]
        cluster_to_label[c] = label

    label_true = []
    label_generated = []
    for i in labels.keys():
        label_true.append(i%(4*2**k))
        label_generated.append(cluster_to_label[labels[i]])

    print(f"Label propagation accuracy : {100*round(accuracy_score(label_true, label_generated),2)}%")
    print(f"Label propagation normalized mutual info score : {round(100*normalized_mutual_info_score(label_true, label_generated),2)}%")
    print(f"Label propagation rand score : {round(100*adjusted_rand_score(label_true, label_generated),2)}%")

if __name__ == "__main__":
    # print(f"Graph considered : {name}")
    # if name == "generate":
    #     p, q = 0.2, 0.001
    #     A = generate_random_graph(p, q, 400,4)
    # else:
    #     print("Load")
    #     t = time.time()
    #     A = load_adjacency_array(name)
    #     print(f"Time : {round(time.time()-t,3)}s\n")
    # if plot:
    #     G = networkx.Graph(A)
    #     networkx.draw(G, node_size=50, arrowsize=5)
    #     plt.show()
    # print("Label Propagation")
    # t = time.time()
    # labels = label_propagation(A)
    # print(f"Time : {round(time.time()-t,3)}s\n")
    # if plot:
    #     labels_values = list(labels.values())
    #     networkx.draw(G, node_size=50, arrowsize=5, node_color=labels_values)
    #     plt.show()

    # export_adjacency_array(A)
    # if not os.path.isdir('louvain'):
    #     os.system('git clone https://github.com/jlguillaume/louvain.git')
    # os.system('cd louvain && make && ./louvain ../output.txt ../louvain_output.txt && cd ..')
    # plot_louvain("generated_graph.txt","louvain_output.txt")
    
    # # from memory_profiler import profile
    
    # i = 5
    # p, q = 0.2, 0.001
    # print("generate")
    # A = generate_random_graph(p, q,(2**i)*400,(2**i)*4)
    # # @profile
    # def experiment(A):
    #     print("propagation")
    #     t = time.time()
    #     labels = label_propagation(A)
    #     print(f"i = {i}, Time : {round(time.time()-t,3)}s\n")
    
    # experiment(A)
    # print("export")
    # export_adjacency_array(A,filename=f"graph_{i}.txt")

    # if not os.path.isdir('LFR-Benchmark_UndirWeightOvp'):
    #     os.system('git clone https://github.com/eXascaleInfolab/LFR-Benchmark_UndirWeightOvp.git')
    # os.system('cd LFR-Benchmark_UndirWeightOvp && make && cd ..')

    k=6
    compute_louvain_scores(k)
    compute_label_prop_scores(k)