from tp1 import  load_adjacency_array, load_adjacency_array_subgraph
import time
import matplotlib.pyplot as plt

name = "scholar" #"amazon"

def core_decomp(network):
    V = {x:len(network[x]) for x in network.keys()} # dictionary for degree per node
    i = len(network.keys()) # number of nodes
    avg_deg_dens = sum(V.values())/(2*i) # average degree density
    max_dens = avg_deg_dens
    c = 0
    eta_rev = dict()
    core_values = dict()

    while bool(V):
        temp = min(V.values()) 
        res = [key for key in V if V[key] == temp] 
        c = max(c, temp)

        new_dens = sum(V.values())/(2*i)
        if new_dens > max_dens:
                max_dens = new_dens
                i_max = i

        for node in res:
            core_values[node] = c
            for neighbour in network[node]:
                network[neighbour].remove(node)
                V[neighbour] -= 1
            V.pop(node)
            network.pop(node)
            eta_rev[i] = node
            i -= 1

    return c, core_values, i_max, eta_rev

if __name__ == "__main__":
    print(f"Graph considered : {name}")
    print("Load...")
    t = time.time()
    A = load_adjacency_array(name)
    print(f"Time : {round(time.time()-t,3)}s\n")

    print("Core decomposition...")
    t = time.time()
    core, core_values, i_max, eta_rev = core_decomp(A)
    
    # nodes of the densest prefix
    densest_pref_nodes = []
    for i in range(1,i_max+1):
        densest_pref_nodes.append(eta_rev[i])
    
    # load and compute the subgraph of A with only the nodes of the prefix
    densest_pref = load_adjacency_array_subgraph(name,densest_pref_nodes) #we have to reload the graph because core_decomp has popped every item of it.
    
    V = {x:len(densest_pref[x]) for x in densest_pref.keys()} # dictionary for degree per node
    avg_deg_dens = sum(V.values())/(2*i_max) # average degree density
    edge_dens = (sum(V.values())/2)/(i_max*(i_max-1)/2) # edge density
    
    print(f"Time : {round(time.time()-t,3)}s\n")
    print(f"Results:\n\
            Core value of the graph: {core}\n\
            Average degree density: {avg_deg_dens}\n\
            Edge density: {edge_dens}\n\
            Size of a densest core ordering prefix: {i_max}")

    if name == "scholar":
        print("Graph mining...")
        A = load_adjacency_array(name)
        nodes = list(A.keys())
        degree = [len(A[k]) for k in nodes]
        coreness = [core_values[k] for k in nodes]
        plt.scatter(degree, coreness)
        plt.xlabel("Degree")
        plt.ylabel("Coreness")
        plt.show()
        plt.savefig("scholar.png", dpi = 300)

        authors_id_file = open("../graph/scholar/ID.txt", encoding='utf-8').read().splitlines()
        matches = [i for i, x in enumerate(coreness) if x>13] # find nodes with coreness > 13
        anomalous_authors = [x for i, x in enumerate(authors_id_file) if i in matches]
        print(f"Anomalous authors with coreness > 13:\n{anomalous_authors}")
