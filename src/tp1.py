# -*- coding: utf-8 -*-
"""
Created on Wed Feb 17 11:10:37 2021

@author: Gaspard
"""

import numpy as np
from collections import deque
import time

name = "lj"
nb_nodes = {"amazon": 334863, "lj": 3997962, "orkut": 3072441}


def count(name):
    s = set()
    i = 0
    with open(f"../graph/com-{name}.ungraph.txt") as f:
        for line in f:
            if i >= 4:
                deb, fin = map(int, line.split())
                s.add(deb)
                s.add(fin)
            i += 1

    print(f"nodes : {len(s)}, edges : {i-4}")


def load_list_edge(name):
    list_edge = []
    i = 0
    with open(f"../graph/com-{name}.ungraph.txt") as f:
        for line in f:
            if i >= 4:
                deb, fin = map(int, line.split())
                list_edge.append((deb, fin))
                list_edge.append((fin, deb))
            i += 1
    return list_edge


def load_adjacency_matrix(name):
    n = nb_nodes[name]
    adjacency_matrix = np.zeros((n, n), dtype=bool)
    i = 0
    with open(f"../graph/com-{name}.ungraph.txt") as f:
        for line in f:
            if i >= 4:
                deb, fin = map(int, line.split())
                adjacency_matrix[deb, fin] = 1
                adjacency_matrix[fin, deb] = 1
            i += 1

    return adjacency_matrix


def load_adjacency_array(name):
    adjacency_array = dict()
    i = 0
    if name in ["amazon", "lj", "orkut", "friendster"]:
        filename = f"../graph/com-{name}.ungraph.txt"
        skip = 4
    elif name == "scholar":
        filename = "../graph/scholar/net.txt"
        skip = 0
    elif name == "wikipedia":
        filename = "../graph/alr21--dirLinks--enwiki-20071018.txt"
        skip = 5
    else:
        filename = name
        skip = 0
    with open(filename) as f:
        for line in f:
            if i >= skip:
                deb, fin = map(int, line.split())
                adjacency_array.setdefault(deb, [])
                adjacency_array[deb].append(fin)
                adjacency_array.setdefault(fin, [])
                adjacency_array[fin].append(deb)
            i += 1
    return adjacency_array

def load_adjacency_array_subgraph(name, nodes):
    adjacency_array = dict()
    i = 0
    if name in ["amazon", "lj", "orkut", "friendster"]:
        filename = f"../graph/com-{name}.ungraph.txt"
        skip = 4
    elif name == "scholar":
        filename = "../graph/scholar/net.txt"
        skip = 0
    elif name == "wikipedia":
        filename = "../graph/alr21--dirLinks--enwiki-20071018.txt"
        skip = 5
    with open(filename) as f:
        for line in f:
            if i >= skip:
                deb, fin = map(int, line.split())
                if (deb in nodes) and (fin in nodes):
                    adjacency_array.setdefault(deb, [])
                    adjacency_array[deb].append(fin)
                    adjacency_array.setdefault(fin, [])
                    adjacency_array[fin].append(deb)
            i += 1
    return adjacency_array


def BFS(adjacency_array, s):
    fifo = deque()
    mark = dict()
    u = s
    mark[s] = 0
    fifo.append(s)
    while len(fifo) != 0:
        prev = u
        u = fifo.popleft()
        m = mark[u]
        neighbors = adjacency_array[u]
        for v in neighbors:
            if not v in mark:
                mark.setdefault(v, m + 1)
                fifo.append(v)
    return mark, u, prev


def estimate_lower_bound(adjacency_array, s):
    previous = [s]
    while True:
        mark, s, _ = BFS(A, s)

        if s in previous:
            break
        else:
            previous.append(s)
    print("Lower bound : ", mark[s], "\n")
    return mark, mark[s]


def estimate_upper_bound(adjacency_array, s):
    mark, s, prev = BFS(A, s)
    print("Upper bound : ", mark[s] + mark[prev], "\n")


def estimate_upper_bound2(mark, lower_bound):
    for k in mark.keys():
        if mark[k] == lower_bound // 2:
            break
    mark, s, prev = BFS(A, k)
    print("Upper bound (2) : ", mark[s] + mark[prev], "\n")


def estimate_upper_bound3(mark, lower_bound):
    mid = list(mark.keys())[len(mark) // 2]
    mark, s, prev = BFS(A, mid)
    print("Upper bound (3) : ", mark[s] + mark[prev], "\n")


def count_triangle(adjacency_array):
    count = 0
    for u in adjacency_array:
        u_neighbors = adjacency_array[u]
        u_set = set(u_neighbors)
        for v in u_neighbors:
            v_neighbors = adjacency_array[v]
            inter = list(u_set & set(v_neighbors))
            count += len(inter)

    print("Triangles : ", count // 6)


def re_index(adjacency_array):
    nodes = list(adjacency_array.keys())
    degree = [len(adjacency_array[k]) for k in nodes]

    sort = np.argsort(degree)

    old_to_new = {}
    new_to_old = {}
    i = 0
    for i, _ in enumerate(sort):
        old_to_new[nodes[sort[i]]] = i
        new_to_old[i] = nodes[sort[i]]

    new_adjacency_matrix = {}
    for new in range(i + 1):
        l = []
        for old in adjacency_array[new_to_old[new]]:
            l.append(old_to_new[old])
        l.sort()
        new_adjacency_matrix[new] = l
    return new_adjacency_matrix


def fast_count_triangle(sorted_adjacency_array):
    count = 0
    tsl = {}
    for u in sorted_adjacency_array:
        tsl[u] = [x for x in sorted_adjacency_array[u] if x > u]

    for u in sorted_adjacency_array:
        u_set = set(tsl[u])
        for v in tsl[u]:
            inter = list(u_set & set(tsl[v]))
            count += len(inter)
    print("Triangles : ", count)


if __name__ == "__main__":
    print(f"Graph considered : {name}")
    print("Load")
    t = time.time()
    A = load_adjacency_array(name)
    print(f"Time : {round(time.time()-t,3)}s\n")

    print("BFS")
    t = time.time()
    mark, last, _ = BFS(A, 1)
    print(f"Time : {round(time.time()-t,3)}s\n")

    t = time.time()
    mark, lower_bound = estimate_lower_bound(A, 1)
    print(f"Time : {round(time.time()-t,3)}s\n")
    
    estimate_upper_bound(A, 1)
    estimate_upper_bound2(mark, lower_bound)
    estimate_upper_bound3(mark, lower_bound)

    print("Count triangles")
    t = time.time()
    count_triangle(A)
    print(f"Time : {round(time.time()-t,3)}s\n")

    A_sorted = re_index(A)

    print("Fast count triangles")
    t = time.time()
    fast_count_triangle(A_sorted)
    print(f"Time : {round(time.time()-t,3)}s\n")