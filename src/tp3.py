import numpy as np
import time
from scipy.sparse import csr_matrix
import matplotlib.pyplot as plt
from tp1 import load_adjacency_array
import pickle

name = "wikipedia"

plot = False

if name in ["amazon", "lj", "orkut", "friendster"]:
    filename = f"../graph/com-{name}.ungraph.txt"
    skip = 4
elif name == "scholar":
    filename = "../graph/scholar/net.txt"
    skip = 0
elif name == "wikipedia":
    filename = "../graph/alr21--dirLinks--enwiki-20071018.txt"
    skip = 5


def associate_wiki_names():
    node2name = {}
    with open("../graph/alr21--pageNum2Name--enwiki-20071018.txt",encoding="utf8") as f:
        i = 0
        for line in f:
            if i >= skip:
                node, name = line.split(maxsplit=1)
                node2name[node] = name
            i += 1
    return node2name


def pagerank(graph, alpha=0.15, t=15):
    n = len(graph)
    node2ind = {node: index for index, node in enumerate(graph)}
    d_out = np.array([1 / len(graph[node]) if graph[node] else 0 for node in graph])
    P = np.ones(n) / n
    for _ in range(t):
        tmp = np.zeros(n)
        with open(filename) as f:
            i = 0
            for line in f:
                if i >= skip:
                    deb, fin = map(int, line.split())
                    tmp[node2ind[fin]] += d_out[node2ind[deb]] * P[node2ind[deb]]
                i += 1
        P = (1 - alpha) * tmp + alpha / n * np.ones(n)
        P = normalize2(P)
    return P


def normalize2(P, P0=None):
    sum_P = P.sum()
    if P0 is None:
        return P + (1 - sum_P) / P.shape[0]
    else:
        return P + P0 * (1 - sum_P) / P.shape[0]


def personalized_pagerank(graph, P0, alpha=0.15, t=15):
    n = len(graph)
    node2ind = {node: index for index, node in enumerate(graph)}
    d_out = np.array([1 / len(graph[node]) if graph[node] else 0 for node in graph])
    P = np.ones(n) / n
    for _ in range(t):
        tmp = np.zeros(n)
        with open(filename) as f:
            i = 0
            for line in f:
                if i >= skip:
                    deb, fin = map(int, line.split())
                    tmp[node2ind[fin]] += d_out[node2ind[deb]] * P[node2ind[deb]]
                i += 1
        P = (1 - alpha) * tmp + alpha / n * P0
        P = normalize2(P, P0)
    return P

if __name__ == "__main__":
    print(f"Graph considered : {name}")
    print("Load")
    t = time.time()
    A = load_adjacency_array(name)

    print(f"Time : {round(time.time()-t,3)}s\n")
    ind2node = {index: node for index, node in enumerate(A)}
    node2ind = {node: index for index, node in enumerate(A)}

    print("PageRank")
    t = time.time()
    P = pagerank(A)
    print(f"Time : {round(time.time()-t,3)}s\n")

    if name == "wikipedia":
        n_top = 5
        node2name = associate_wiki_names()
        top_pages = np.argsort(P)[-n_top:]
        print(f"Top {n_top} pages")
        for i in range(n_top):
            node = ind2node[top_pages[i]]
            print(f"  {node2name[str(node)]}  : score = {round(P[top_pages[i]],4)}")
        last_pages = np.argsort(P)[:n_top]
        print(f"Last {n_top} pages")
        for i in range(n_top):
            node = ind2node[last_pages[i]]
            print(f"  {node2name[str(node)]}  : score = {round(P[last_pages[i]],4)}")

    print(f"Personnalized Pagerank")
    t = time.time()
    P = personalized_pagerank(A, P0=P)
    print(f"Time : {round(time.time()-t,3)}s\n")

    if plot:
        P_benchmark = {"0.15": P}
        print("Benchmark on alpha parameter")
        for alpha in [0.1, 0.2, 0.5, 0.9]:
            print(f"  alpha: {alpha}")
            P_benchmark[str(alpha)] = pagerank(A, alpha=alpha)
        with open('../plots/P_benchmark.pkl', 'wb') as f:
            pickle.dump(P_benchmark, f)
        degrees = {"out": np.array([len(A[node]) for node in A])}
        degrees["in"] = np.zeros(len(A))
        for node in A:
            for dest in A[node]:
                degrees["in"][node2ind[dest]] += 1
        # degrees["in"] /= len(A)

        with open('../plots/degrees.pkl', 'wb') as f:
            pickle.dump(degrees, f)


        for deg in ["in", "out"]:
            for scale in [("linear", "linear"),]: # ("linear", "log"), ("log", "linear"), ("log", "log")]:
                fig, ax = plt.subplots()
                ax.scatter(P, degrees[deg], marker='.')#, alpha=0.5)
                ax.set_xlabel("PageRank alpha=0.15")
                ax.set_xscale(scale[0])
                if scale[0] == 'log':
                    pass
                    #ax.set_xlim(np.min(np.log(P), 0), np.max(np.log(P)))
                else:
                    ax.set_xlim(0, np.max(P))
                ax.set_ylabel(f"{deg}-degrees")
                ax.set_yscale(scale[1])
                if scale[1] == 'log':
                    pass
                    #ax.set_ylim(np.min(np.log(new_degrees[deg])), np.max(np.log(new_degrees[deg])))
                else:
                    pass
                    #ax.set_ylim(0, np.max(new_degrees[deg]))
                #ax.set_ylim(min(new_degrees[deg]), max(new_degrees[deg]))
                ax.set_title(f'{deg}_degrees on {scale[0]}_{scale[1]} scale')
                fig.savefig(f"../plots/{deg}_degrees_{scale[0]}_{scale[1]}.png")

        for alpha in P_benchmark:
            for scale in [("linear", "linear"), ]: # ("linear", "log"), ("log", "linear"), ("log", "log")]:
                fig, ax = plt.subplots()
                ax.scatter(P, P_benchmark[str(alpha)], marker='.')#, alpha=0.5)
                ax.set_xlabel("PageRank alpha=0.15")
                ax.set_xscale(scale[0])
                if scale[0] == 'log':
                    pass
                    #ax.set_xlim(np.min(np.log(P), 0), np.max(np.log(P)))
                else:
                    ax.set_xlim(0, np.max(P))
                ax.set_ylabel(f"PageRank alpha={alpha}")
                ax.set_yscale(scale[1])
                if scale[1] == 'log':
                    pass
                    #ax.set_ylim(min(np.log(P_benchmark[str(alpha)])), max(np.log(P_benchmark[str(alpha)])))
                else:
                    pass
                    #ax.set_ylim(0, max(P_benchmark[str(alpha)]))
                #ax.set_ylim(min(P_benchmark[str(alpha)]), max(P_benchmark[str(alpha)]))
                ax.set_title(f'pagerank_{alpha} on {scale[0]}_{scale[1]} scale')
                fig.savefig(f"../plots/pagerank_{alpha}_{scale[0]}_{scale[1]}.png")

